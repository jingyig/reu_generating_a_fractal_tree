function convertToAbsolute(path){
    var x0,y0,x1,y1,x2,y2,segs = path.pathSegList;
    for (var x=0,y=0,i=0,len=segs.numberOfItems;i<len;++i){
      var seg = segs.getItem(i), c=seg.pathSegTypeAsLetter;
      if (/[MLHVCSQTA]/.test(c)){
        if ('x' in seg) x=seg.x;
        if ('y' in seg) y=seg.y;
      }else{
        if ('x1' in seg) x1=x+seg.x1;
        if ('x2' in seg) x2=x+seg.x2;
        if ('y1' in seg) y1=y+seg.y1;
        if ('y2' in seg) y2=y+seg.y2;
        if ('x'  in seg) x+=seg.x;
        if ('y'  in seg) y+=seg.y;
        switch(c){
          case 'm': segs.replaceItem(path.createSVGPathSegMovetoAbs(x,y),i);                   break;
          case 'l': segs.replaceItem(path.createSVGPathSegLinetoAbs(x,y),i);                   break;
          case 'h': segs.replaceItem(path.createSVGPathSegLinetoHorizontalAbs(x),i);           break;
          case 'v': segs.replaceItem(path.createSVGPathSegLinetoVerticalAbs(y),i);             break;
          case 'c': segs.replaceItem(path.createSVGPathSegCurvetoCubicAbs(x,y,x1,y1,x2,y2),i); break;
          case 's': segs.replaceItem(path.createSVGPathSegCurvetoCubicSmoothAbs(x,y,x2,y2),i); break;
          case 'q': segs.replaceItem(path.createSVGPathSegCurvetoQuadraticAbs(x,y,x1,y1),i);   break;
          case 't': segs.replaceItem(path.createSVGPathSegCurvetoQuadraticSmoothAbs(x,y),i);   break;
          case 'a': segs.replaceItem(path.createSVGPathSegArcAbs(x,y,seg.r1,seg.r2,seg.angle,seg.largeArcFlag,seg.sweepFlag),i);   break;
          case 'z': case 'Z': x=x0; y=y0; break;
        }
      }
      // Record the start of a subpath
      if (c=='M' || c=='m') x0=x, y0=y;
    }
} 
var svg = document.childNodes[1].childNodes[2].childNodes[3];
var path = document.createElementNS("http://www.w3.org/2000/svg", "path");
  
var vxs_str_m = "m 53.672618,99.02976 15.119048,-30.994046 45.357144,-14.363096 49.89285,11.339287 31.75,27.214285 -6.04761,49.89286 -63.5,58.96428 -51.404766,-3.77976 -24.946427,-30.2381 z";
var vxs_str_M = "M 53.6726 99.0298 L 68.7917 68.0357 L 114.149 53.6726 L 164.042 65.0119 L 195.792 92.2262 L 189.744 142.119 L 126.244 201.083 L 74.8393 197.304 L 49.8929 167.065 Z";
var vxs_str_2 = "m 77.270167,230.13071 -17.357791,26.87658 113.665534,29.1163 -46.47408,-45.91416 -8.39894,-23.51701 -29.676219,2.23972 z";
// Create a path object
path.setAttribute("d", vxs_str_m);
path.setAttribute("style", "fill:none;stroke:#000000;stroke-width:0.264583px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1");
convertToAbsolute(path);
svg.appendChild(path);

// get the arr of vxs
var mystring = path.getAttribute('d');
var arr = mystring.split(" ").filter(function (st) { return(st.length > 1) }).map(function(x) { return(x * 1)});
var arr_of_arr = [];

// set xmin xmax ymin ymax to prepare for boundingbx boarder
var xmin = arr[0];
var xmax = arr[0];
var ymin = -arr[1];
var ymax = -arr[1];

for(i = 0; i < arr.length; ++i) {
    if (arr[i] < xmin) {
        xmin = arr[i];
    }
    if (arr[i] > xmax) {
        xmax = arr[i];
    }
    var pair = [arr[i++], -arr[i]];
    if (-arr[i] < ymin) {
        ymin = -arr[i];
    }
    if (-arr[i] > ymax) {
        ymax = -arr[i];
    }
    arr_of_arr.push(pair);
}

var b1 = JXG.JSXGraph.initBoard('jxgbox', { grid: true
                                            , boundingbox: [xmin - 10
                                                            , ymax + 10
                                                            , xmax + 10
                                                            , ymin - 10]
                                            , keepaspectratio: true});

  

// An array containing four numbers describing the left
// , top, right and bottom boundary of the board in user coordinates

// // Create points objects in order to visualize their order
// for(i = 0; i < arr_of_arr.length; ++i) {
//     var name_of_pt = "p";
//     name_of_pt = name_of_pt.concat(i);
//     var name_of_pt = b1.create('point'
//                          , arr_of_arr[i]
//                          , {name: name_of_pt, size: 4});
// }   

var poly = b1.create('polygon', arr_of_arr, {hasInnerPoints: true});
var i = 0;
for(i = 0; i < poly.vertices.length - 1; ++i) {
    var str = "p";
    str = str.concat(i);
    poly.vertices[i].setLabel(str);
}

var tri1 = b1.create('polygon', [poly.vertices[0]
                                 , poly.vertices[4]
                                 , poly.vertices[8]
                                 , poly.vertices[5]
                                 , poly.vertices[2]
                                 , poly.vertices[3]], {hasInnerPoints: true, color: 'pink'});
// var tri1 = b1.create('polygon', [[poly.vertices[0].X(),poly.vertices[0].Y()]
//                                  , [poly.vertices[4].X(),poly.vertices[4].Y()]
//                                  , [poly.vertices[8].X(),poly.vertices[8].Y()]
//                                  , [poly.vertices[5].X(),poly.vertices[5].Y()]
//                                  , [poly.vertices[2].X(),poly.vertices[2].Y()]
//                                  , [poly.vertices[3].X(),poly.vertices[3].Y()]]
//                                  , {hasInnerPoints: true
//                                     , color: 'pink'});
var tri2 = b1.create('polygon', [[poly.vertices[2].X(),poly.vertices[2].Y()]
                                  , [poly.vertices[3].X(),poly.vertices[3].Y()]
                                  , [poly.vertices[2].X(),poly.vertices[2].Y()]
                                  , [poly.vertices[4].X(),poly.vertices[4].Y()]]
                              , {hasInnerPoints: true, color: 'blue'});
// poly.vertices.map(function(x){var str = "p"; str = str.concat(i); x.setLabel(str); i++;});
// poly.vertices.map(function(x){ b1.removeObject(x)});
// b1.removeObject(tri1);
// tri1.vertices = 
// var arr = [];
// poly.vertices.pop();
// arr = JXG.Math.Geometry.sortVertices(poly.vertices);
// b1.removeObject(poly);

// var sortColckwise_1stversion = function (poly) {

//     var ori = poly.vertices[0];
//     var temp = [];
//     for(i = 1; i < poly.vertices.length - 1; ++i) {
//         temp.push(poly.vertices[i]);
//     }
//     temp.sort(function(a,b) {
//                   var rad1 = Math.atan2(a.Y() - ori.Y(), a.X() - ori.X());
//                       rad2 = Math.atan2(b.Y() - ori.Y(), b.X() - ori.X());
//                   return rad2 - rad1;
//     });
//     temp.push(ori);
//     b1.removeObject(poly);
//     poly = b1.create('polygon', temp);
// }


var sortColckwise_and_rotate = function (poly, ori) {
    // find a interior point
    var int_x = 1/3 * (poly.vertices[0].X() + poly.vertices[1].X() + poly.vertices[2].X());
    var int_y = 1/3 * (poly.vertices[0].Y() + poly.vertices[1].Y() + poly.vertices[2].Y());
    // var temp = poly.vertices.slice();
    // temp.pop();
    var temp = [];
    for (i = 0; i < poly.vertices.length - 1; ++i) {
        var pair = [poly.vertices[i].X(), poly.vertices[i].Y()];
        temp.push(pair);
    }

    var ref = Math.atan2(ori.Y() - int_y, ori.X() - int_x);
    
    temp.sort(function(a,b) {
                  var rad1 = (Math.atan2(a[1] - int_y, a[0] - int_x) - ref + 6 * Math.PI) % (2 * Math.PI);
                      rad2 = (Math.atan2(b[1] - int_y, b[0] - int_x) - ref + 6 * Math.PI) % (2 * Math.PI);
                  return rad2 - rad1;
    });

    var vic = temp[temp.length - 1].slice();
    temp.splice(temp.length - 1, 1);
    temp.splice(0, 0, vic);
    

    // testing
    var diff = [];
    var oridiff = [];
    for (i = 0; i < temp.length; ++i) {
        diff.push((Math.atan2(temp[i][1] - int_y, temp[i][0] - int_x)- ref + 6 * Math.PI) % (2 * Math.PI));
    }
    for (i = 0; i < temp.length; ++i) {
        oridiff.push((Math.atan2(temp[i][1] - int_y, temp[i][0] - int_x)));
    }

    window.arrori_diff = oridiff;
    window.arrtemp = temp;
    window.arrdiff = diff;

    b1.removeObject(poly);
    // poly.vertices =  temp;
    poly = b1.create('polygon', temp, {hasInnerPoints: true, color: 'orange'});
    return poly;
}

tri1 = sortColckwise_and_rotate(tri1, poly.vertices[4]);
tri2 = sortColckwise_and_rotate(tri2, poly.vertices[2]);
// sortColckwise_and_rotate(tri1, poly.vertices[4]);

// return the polygon with vxs sorted clockwise
var sortClockwise = function (poly) {
    // find a interior point
    var int_x = 1/3 * (poly.vertices[0].X() + poly.vertices[1].X() + poly.vertices[2].X());
    var int_y = 1/3 * (poly.vertices[0].Y() + poly.vertices[1].Y() + poly.vertices[2].Y());
    // var ori = poly.vertices[0];
    // var temp = poly.vertices.slice();
    // temp.pop();
    var temp = [];
    for (i = 0; i < poly.vertices.length - 1; ++i) {
        var pair = [poly.vertices[i].X(), poly.vertices[i].Y()];
        temp.push(pair);
    }

    temp.sort(function(a,b) {
                  var rad1 = Math.atan2(a[1] - int_y, a[0] - int_x);
                      rad2 = Math.atan2(b[1] - int_y, b[0] - int_x);
                  return rad2 - rad1;
    });

    window.arrtemp = temp;
    // var vic = temp[0];
    // temp.splice(0,1);
    // temp.push(vic);
    // poly.vertices = temp;

    // b1.removeObject(poly);
    poly = b1.create('polygon', temp, {color: 'orange'});
    // return poly;
}//sortClockwise

// return the polygon with vxs sorted clockwise
var rotate_vxs = function (poly, vx) {
    var temp = poly.vertices.slice();
    temp.pop();
    while ((temp[0].X() != vx.X()) && (temp[0].Y() != vx.Y()) ) {
        var victim = temp[0];
        temp.splice(0, 1);
        temp.push(victim);
    }
    // b1.removeObject(poly);
    poly = b1.create('polygon', temp, {color: 'purple'});
    // return poly;
}//rotate_vxs

var sortClockwise_arr = function (arr) {
    var int_x = 1/3 * (arr[0][0] + arr[1][0] + arr[2][0]);
    var int_y = 1/3 * (arr[0][1] + arr[1][1] + arr[2][1]);
    // var ori = arr[0];
    // arr.splice(0,1);
    arr = arr.sort(function(a,b) {
                  var rad1 = Math.atan2(a[1] - int_y, a[0] - int_x);
                      rad2 = Math.atan2(b[1] - int_y, b[0] - int_x);
                  return rad2 - rad1;
    });
}//sortClockwise_arr

// rotate the vxs in input array with desired vx in the first position
var rotate_vxs_arr = function (arr, vx) {
    while ((arr[0][0] != vx.X()) && (arr[0][1] != vx.Y()) ) {
        var victim = arr[0];
        arr.splice(0, 1);
        arr.push(victim);
    }
}//rotate_vxs_arr


var start_ep_1_no = 2;
var start_ep_2_no = (start_ep_1_no + 1) % arr_of_arr.length;
var gamma = 0.7;

var stem_x_coord = gamma * poly.vertices[start_ep_2_no].X() + (1 - gamma) * poly.vertices[start_ep_1_no].X();
var stem_y_coord = gamma * poly.vertices[start_ep_2_no].Y() + (1 - gamma) * poly.vertices[start_ep_1_no].Y();

var stem = b1.create('point', [stem_x_coord, stem_y_coord]
                            , {name: 'Stem', size: 6, color: 'blue'});

var arr_of_left_poly = [];
var arr_of_right_poly = arr_of_arr.slice();
arr_of_right_poly.splice(start_ep_2_no, 1);
arr_of_right_poly.splice(0, 0, [stem.X(), stem.Y()]);
sortClockwise_arr(arr_of_right_poly);
rotate_vxs_arr(arr_of_right_poly, stem);
arr_of_left_poly.push([stem.X(), stem.Y()]);
arr_of_left_poly.push([poly.vertices[start_ep_2_no].X(), poly.vertices[start_ep_2_no].Y()]);
var target;

// tri1 = sortClockwise(tri1);
// tri1 = rotate_vxs(tri1, tri1.vertices[3]);


tri1 = b1.create('polygon', [[poly.vertices[0].X(),poly.vertices[0].Y()]
                              , [poly.vertices[4].X(),poly.vertices[4].Y()]
                              , [poly.vertices[8].X(),poly.vertices[8].Y()]]
                          , {hasInnerPoints: true, color: 'pink'});

tri1.setLabel("hihihihiihihi");
