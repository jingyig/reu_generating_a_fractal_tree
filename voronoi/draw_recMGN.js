// Transfer relative coordinate into absolute coordinate
function convertToAbsolute(path){
    var x0,y0,x1,y1,x2,y2,segs = path.pathSegList;
    for (var x=0,y=0,i=0,len=segs.numberOfItems;i<len;++i){
      var seg = segs.getItem(i), c=seg.pathSegTypeAsLetter;
      if (/[MLHVCSQTA]/.test(c)){
        if ('x' in seg) x=seg.x;
        if ('y' in seg) y=seg.y;
      }else{
        if ('x1' in seg) x1=x+seg.x1;
        if ('x2' in seg) x2=x+seg.x2;
        if ('y1' in seg) y1=y+seg.y1;
        if ('y2' in seg) y2=y+seg.y2;
        if ('x'  in seg) x+=seg.x;
        if ('y'  in seg) y+=seg.y;
        switch(c){
          case 'm': segs.replaceItem(path.createSVGPathSegMovetoAbs(x,y),i);                   break;
          case 'l': segs.replaceItem(path.createSVGPathSegLinetoAbs(x,y),i);                   break;
          case 'h': segs.replaceItem(path.createSVGPathSegLinetoHorizontalAbs(x),i);           break;
          case 'v': segs.replaceItem(path.createSVGPathSegLinetoVerticalAbs(y),i);             break;
          case 'c': segs.replaceItem(path.createSVGPathSegCurvetoCubicAbs(x,y,x1,y1,x2,y2),i); break;
          case 's': segs.replaceItem(path.createSVGPathSegCurvetoCubicSmoothAbs(x,y,x2,y2),i); break;
          case 'q': segs.replaceItem(path.createSVGPathSegCurvetoQuadraticAbs(x,y,x1,y1),i);   break;
          case 't': segs.replaceItem(path.createSVGPathSegCurvetoQuadraticSmoothAbs(x,y),i);   break;
          case 'a': segs.replaceItem(path.createSVGPathSegArcAbs(x,y,seg.r1,seg.r2,seg.angle,seg.largeArcFlag,seg.sweepFlag),i);   break;
          case 'z': case 'Z': x=x0; y=y0; break;
        }
      }
      // Record the start of a subpath
      if (c=='M' || c=='m') x0=x, y0=y;
    }
}  
  
var svg = document.childNodes[1].childNodes[2].childNodes[3];
var path = document.createElementNS("http://www.w3.org/2000/svg", "path");
  
var vxs_str_m = "m 53.672618,99.02976 15.119048,-30.994046 45.357144,-14.363096 49.89285,11.339287 31.75,27.214285 -6.04761,49.89286 -63.5,58.96428 -51.404766,-3.77976 -24.946427,-30.2381 z";
var vxs_str_2 = "m 77.270167,230.13071 -17.357791,26.87658 113.665534,29.1163 -46.47408,-45.91416 -8.39894,-23.51701 -29.676219,2.23972 z";
var vxs_str_3 = "m 87.65003,13.48462 -44.419924,21.020142 -6.742311,11.501588 9.12195,9.12195 67.423095,2.776246 11.8982,-12.294801 -10.31177,-22.60657 z";
var vxs_str_4 = "m 35.52976,110.36905 97.51786,-51.404766 27.21428,9.07143 35.52976,73.327376 -46.11309,96.00596 -33.2619,13.60714 -40.821432,-6.04762 -41.577381,-43.08929 z"
var vxs_str_5 = "m 27.970239,106.58928 65.011902,-51.404757 63.499999,15.119049 48.38095,120.952378 -20.41071,15.875 -61.23214,14.3631 z"
// Create a path object
path.setAttribute("d", vxs_str_m);
path.setAttribute("style", "fill:none;stroke:#000000;stroke-width:0.264583px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1");
//convertToAbsolute(path);
svg.appendChild(path);

// get the arr of vxs
var mystring = path.getAttribute('d');
// var mystring = "M 87.65 13.4846 L 43.2301 34.5048 L 36.4878 46.0063 L 45.6097 55.1283 L 113.033 57.9045 L 124.931 45.6097 L 114.619 23.0032 Z"
var mystring = "M 53.6726 99.0298 L 68.7917 68.0357 L 114.149 53.6726 L 164.042 65.0119 L 195.792 92.2262 L 189.744 142.119 L 126.244 201.083 L 74.8393 197.304 L 49.8929 167.065 Z"
// var mystring = "M 35.5298 110.369 L 133.048 58.9643 L 160.262 68.0357 L 195.792 141.363 L 149.679 237.369 L 116.417 250.976 L 75.5952 244.929 L 34.0179 201.839 Z"
// var mystring = "M 27.9702 106.589 L 92.9821 55.1845 L 156.482 70.3036 L 204.863 191.256 L 184.452 207.131 L 123.22 221.494 Z";
var arr = mystring.split(" ").filter(function (st) { return(st.length > 1) }).map(function(x) { return(x * 1)});
var arr_of_arr = [];

// set xmin xmax ymin ymax to prepare for boundingbx boarder
var xmin = arr[0];
var xmax = arr[0];
var ymin = -arr[1];
var ymax = -arr[1];

for(i = 0; i < arr.length; ++i) {
    if (arr[i] < xmin) {
        xmin = arr[i];
    }
    if (arr[i] > xmax) {
        xmax = arr[i];
    }
    var pair = [arr[i++], -arr[i]];
    if (-arr[i] < ymin) {
        ymin = -arr[i];
    }
    if (-arr[i] > ymax) {
        ymax = -arr[i];
    }
    arr_of_arr.push(pair);
}

var b1 = JXG.JSXGraph.initBoard('jxgbox', { grid: true
                                            , boundingbox: [xmin - 10
                                                            , ymax + 10
                                                            , xmax + 10
                                                            , ymin - 10]
                                            , keepaspectratio: true});

  

// An array containing four numbers describing the left
// , top, right and bottom boundary of the board in user coordinates


var poly = b1.create('polygon', arr_of_arr, {hasInnerPoints: true});
// Label vertices
for(i = 0; i < poly.vertices.length - 1; ++i) {
    var str = "p";
    str = str.concat(i);
    poly.vertices[i].setLabel(str);
}

// Set the stem point
var start_ep_1_no = 1;//2;3;7
var start_ep_2_no = (start_ep_1_no + 1) % arr_of_arr.length;

// var edpt1 = poly.vertices[start_ep_1_no];
// var edpt2 = poly.vertices[start_ep_2_no];
var gamma = 0.5;//0.7;0.4;0.5

var stem_x_coord = gamma * poly.vertices[start_ep_2_no].X() + (1 - gamma) * poly.vertices[start_ep_1_no].X();
var stem_y_coord = gamma * poly.vertices[start_ep_2_no].Y() + (1 - gamma) * poly.vertices[start_ep_1_no].Y();

var stem = b1.create('point', [stem_x_coord, stem_y_coord]
                            , {name: 'Stem', size: 3, color: 'blue'});

var computeCentroid = function (poly_in) {
    var j;
    var tempDet = 0;
    var det = 0;
    var cen_x = 0;
    var cen_y = 0;
    for (i = 0; i < poly_in.vertices.length - 1; ++i) {
        if (i + 1 == poly_in.vertices.length - 1) {
            j = 0;
        } else {
            j = i + 1;
        }
        tempDet = poly_in.vertices[i].X() * poly_in.vertices[j].Y() 
                  - poly_in.vertices[j].X() * poly_in.vertices[i].Y();
        det += tempDet;

        cen_x += (poly_in.vertices[i].X() + poly_in.vertices[j].X()) * tempDet;
        cen_y += (poly_in.vertices[i].Y() + poly_in.vertices[j].Y()) * tempDet;
    }

    cen_x /= 3 * det;
    cen_y /= 3 * det;
    poly_in.centroid = b1.create('point', [cen_x, cen_y]
                                        , {size: 1, color: 'blue'});
    return poly_in.centroid;
}//computeCentroid

var computeCentroidArea = function (poly_in) {
    window.dbcccapoly = poly_in
    // construct triangles
    var tris = [];
    var cen_x = 0;
    var cen_y = 0;
    if (poly_in.vertices.length == 4 ){
        cen_x = 1/3 * (poly_in.vertices[0].X() 
                       + poly_in.vertices[1].X()
                       + poly_in.vertices[2].X());
        cen_y = 1/3 * (poly_in.vertices[0].Y() 
                       + poly_in.vertices[1].Y()
                       + poly_in.vertices[2].Y());
    } else {
        for (i = 1; i < poly_in.vertices.length - 2; ++i) {
            var tri = b1.create('polygon', [poly_in.vertices[0]
                                            , poly_in.vertices[i]
                                            , poly_in.vertices[i+1]], {hasInnerPoints: true
                                                                    , color: 'red'});
            
            // compute the centroid of triangles
            tri_cen_x = 1/3 * (poly_in.vertices[0].X() 
                               + poly_in.vertices[i].X()
                               + poly_in.vertices[i+1].X());
            
            tri_cen_y = 1/3 * (poly_in.vertices[0].Y() 
                               + poly_in.vertices[i].Y()
                               + poly_in.vertices[i+1].Y());
            tri.centroid = [tri_cen_x, tri_cen_y];
    
            tris.push(tri);
        }//for
    
        // add them up, get centroid of poly
        for (i = 0; i < tris.length ; ++i) {
            cen_x += tris[i].centroid[0] * tris[i].Area();
            cen_y += tris[i].centroid[1] * tris[i].Area();
            
        }
    
        cen_x /= poly_in.Area() ;
        cen_y /= poly_in.Area() ;
    }


    // poly_in.centroid = b1.create('point', [cen_x, cen_y]
    //                                     , {name: 'CentroidArea', size: 1, color: 'blue'});
    poly_in.centroid = b1.create('point', [cen_x, cen_y]
                                        , {size: 1, color: 'blue'});
    window.ct = poly_in.centroid;
    // delete all the tris
    for(i = 0; i < tris.length; ++i) {
        //tris[i].vertices.map(function(x){ b1.removeObject(x)});
        b1.removeObject(tris[i]);
    }

    return poly_in.centroid ;
}//conputeCentroidArea

var findNewStem = function(poly_left_in, poly_right_in, stem_in, strokeWidth_in) {
    var ns_x = 1/3 * (poly_left_in.centroid.X() + poly_right_in.centroid.X() + stem_in.X());
    var ns_y = 1/3 * (poly_left_in.centroid.Y() + poly_right_in.centroid.Y() + stem_in.Y());
    // var newStem = b1.create('point', [ns_x, ns_y]
    //                                , {name: 'Stem', size: 6, color: 'blue'});
    var newStem = b1.create('point', [ns_x, ns_y]
                                   , {size: 3, color: 'blue'});
    // create a branch
    var branch = b1.create('segment', [stem_in, newStem],{strokeWidth: strokeWidth_in, color: 'orange'});  
    return newStem; 
}//findNewStem

// return the polygon with vxs sorted clockwise starting with desired vertex ori
// polygon is recommonded to be ocnstructed by point objects rather than coordinates
var sortClockwise_and_rotate = function (poly_in, ori_in, color_in) {
    // find a interior point
    var int_x = 1/3 * (poly_in.vertices[0].X() + poly_in.vertices[1].X() + poly_in.vertices[2].X());
    var int_y = 1/3 * (poly_in.vertices[0].Y() + poly_in.vertices[1].Y() + poly_in.vertices[2].Y());

    var temp = [];
    for (i = 0; i < poly_in.vertices.length - 1; ++i) {
        var pair = [poly_in.vertices[i].X(), poly_in.vertices[i].Y()];
        temp.push(pair);
    }

    // set up the reference angle
    var ref = Math.atan2(ori_in.Y() - int_y, ori_in.X() - int_x);
    
    // sort temp
    temp.sort(function(a,b) {
                  var rad1 = (Math.atan2(a[1] - int_y, a[0] - int_x) - ref + 6 * Math.PI) % (2 * Math.PI);
                      rad2 = (Math.atan2(b[1] - int_y, b[0] - int_x) - ref + 6 * Math.PI) % (2 * Math.PI);
                  return rad2 - rad1;
    });

    // swap the last elt to the first position
    var vic = temp[temp.length - 1].slice();
    temp.splice(temp.length - 1, 1);
    temp.splice(0, 0, vic);
    
    // return the organized new polygon
    b1.removeObject(poly_in);
    poly_in = b1.create('polygon', temp, {hasInnerPoints: true, color: color_in});
    return poly_in;
}//sortClockwise

// sort the vxs in input array clockwise and start with the desired vertex
var sortClockwise_and_rotate_arr = function (arr_in, ori_in) {
    // find a interior point
    var int_x = 1/3 * (arr_in[0][0] + arr_in[1][0] + arr_in[2][0]);
    var int_y = 1/3 * (arr_in[0][1] + arr_in[1][1] + arr_in[2][1]);

    // set up the reference angle
    var ref = Math.atan2(ori_in.Y() - int_y, ori_in.X() - int_x);
    
    // sort temp
    arr_in.sort(function(a,b) {
                  var rad1 = (Math.atan2(a[1] - int_y, a[0] - int_x) - ref + 6 * Math.PI) % (2 * Math.PI);
                      rad2 = (Math.atan2(b[1] - int_y, b[0] - int_x) - ref + 6 * Math.PI) % (2 * Math.PI);
                  return rad2 - rad1;
    });

    // swap the last elt to the first position
    var vic = arr_in[arr_in.length - 1].slice();
    arr_in.splice(arr_in.length - 1, 1);
    arr_in.splice(0, 0, vic);
}

// DEBUGGING
    window.iteration = 0;
    window.flag = 0;
    window.area_too_small = false;
    window.equalzero = false;
    window.finishbisec = false;

var identity = function (x) { return x } ;

var verticesX = function (vertices) { return vertices.map(function (v) { return v.X() }) } ;
var verticesY = function (vertices) { return vertices.map(function (v) { return v.Y() }) } ;

// Computes the angle in radians in [0,2pi] from vertices v1 to v2 about origin
var angle = function(origin, v1, v2) {
  var theta1 = Math.atan2(v1.Y() - origin.Y(), v1.X() - origin.X());
  var theta2 = Math.atan2(v2.Y() - origin.Y(), v2.X() - origin.X());
  return ((theta1 - theta2) + 2*Math.PI) % (2*Math.PI) }

// Sort vertex objects counter-clockwise starting with origin
var sortVerticesMGN = function(vertices, origin) {
  if(vertices.length < 3) { return undefined ; }
  var vs = vertices.map(identity) ; // shallowly copy the array
  
  var sometri = b1.create('polygon', vs.slice(0,3)) ;
  var interior = computeCentroidArea(sometri) ;
  sometri.remove() ;
  
  v2 = vs.sort(function(a,b) {
    return (angle(interior, a, origin) - angle(interior, b, origin)) })

  interior.remove() ;

  return v2 }

var verticesArea = function (vertices) {
  poly = b1.create('polygon', vertices) ;
  area = poly.Area() ;
  poly.remove() ;
  return area }

var bisectPolyMGN = function(poly, stem) {

  // a list of vertices including the stem
  verts = poly.vertices.slice(1).concat(stem) ;
  verts = sortVerticesMGN(verts, stem) ;
  verts = [stem].concat(verts.slice(1).reverse()) ; // flip direction
  nverts = verts.length ;

  var leftVerts = function (index) {
    return verts.slice(0, index + 1) }
  var rightVerts = function (index) {
    return [stem].concat(verts.slice(index, nverts)) }

  var find_left_middle_index = function (init) {
    if (nverts == 3) { return 1 } // a triangle
    if (init > nverts - 2) { return undefined } // prevent infinite recursion

    areaL = verticesArea(leftVerts(init)) ; // keep order CCW
    areaR = verticesArea(rightVerts(init)) ;

    if (areaR <= areaL) { return init - 1 }
    else { return find_left_middle_index(init + 1) } }

  isplit = find_left_middle_index(2) ;
  areaL = ((isplit >= 2) ? verticesArea(leftVerts(isplit)) : 0) ;
  areaR = ((nverts - isplit >= 2) ? verticesArea(rightVerts(isplit)) : 0) ;
  if (areaL == areaR) { // actually happens!
    return [ leftVerts(isplit), rightVerts(isplit) ] }

  areabetween = verticesArea([stem, verts[isplit], verts[isplit + 1]]) ;
  tot = poly.Area() ;
  eta = ((tot/2) - areaL) / areabetween ;
  vL = verts[isplit] ;
  vR = verts[isplit + 1] ;
  interpoint = b1.create('point',
    [ eta * vR.X() + (1 - eta) * vL.X(),
      eta * vR.Y() + (1 - eta) * vL.Y()]) ;
  vertsL = leftVerts(isplit).concat(interpoint) ;
  vertsR = [stem, interpoint].concat(rightVerts(isplit + 1).slice(1)) ;
  return [vertsL, vertsR]
}

var findNewStemMGN = function(stem, centroid, strokewidth) {
  var newStem = b1.create('point', 
    [ (1/3)*stem.X() + (2/3)*centroid.X(), 
      (1/3)*stem.Y() + (2/3)*centroid.Y()],
    {size: 3, color: 'blue'});
  // create a branch
  var branch = b1.create('segment', [stem, newStem],
    {strokeWidth: strokewidth, color: 'orange'});  
  return newStem; 
} 



var bisecPoly = function(poly_in, stem_in, start_ep_2_no_in) {
    // Cut the polygon into half
    arr_of_arr = [];
    for (i = 0; i < poly_in.vertices.length - 1; ++i) {
        arr_of_arr.push([poly_in.vertices[i].X(), poly_in.vertices[i].Y()]);
    }
    window.arr_of_arr_win = arr_of_arr;
    ++flag;

    var stem_coords = [stem_in.X(), stem_in.Y()] ;
    var ep_coords = [
      poly_in.vertices[start_ep_2_no_in].X(),
      poly_in.vertices[start_ep_2_no_in].Y()] ;
    
    // set up original arrays for left and right polygon
    var arr_of_left_poly = [];
    var arr_of_right_poly = arr_of_arr.slice();
    window.arr_of_pr_win_ori = arr_of_right_poly;
    window.arr_of_pl_win_ori = arr_of_left_poly;
    arr_of_right_poly.splice(start_ep_2_no_in, 1);
    arr_of_right_poly.splice(0, 0, stem_coords);
    sortClockwise_and_rotate_arr(arr_of_right_poly, stem_in);
    arr_of_left_poly.push(stem_coords);
    arr_of_left_poly.push(ep_coords);
    var target;
    window.target_win = target;
    // // find target
    for(i = 1; i < arr_of_arr.length - 1; ++i) {
        arr_of_left_poly.push([poly_in.vertices[(start_ep_2_no_in + i) % arr_of_arr.length].X()
                               , poly_in.vertices[(start_ep_2_no_in + i) % arr_of_arr.length].Y()]);
        if(i > 1) {
            arr_of_right_poly.splice(1, 1);
        }
    
        var poly_left = b1.create('polygon', arr_of_left_poly, {hasInnerPoints: true, color: 'pink'});
        var poly_right = b1.create('polygon', arr_of_right_poly, {hasInnerPoints: true, color: 'blue'});
        
        if(poly_left.Area() >= poly_in.Area() / 2){
            target = (start_ep_2_no_in + i) % arr_of_arr.length;
            break;
        } else {
            poly_left.vertices.map(function(x){ b1.removeObject(x)});
            poly_right.vertices.map(function(x){ b1.removeObject(x)});
            b1.removeObject(poly_left);
            b1.removeObject(poly_right);
        }
    }//for
    
    // // find the final intersection point on the line
    if(poly_left.Area() > poly_in.Area() / 2) {
        var tri = b1.create('polygon', [stem_in, poly_in.vertices[target]
                                        , poly_in.vertices[(target + arr_of_arr.length - 1) % arr_of_arr.length]]);
        var frac = (poly_left.Area() - poly_in.Area() / 2) / (tri.Area());
    
        var int1_x = frac * poly_in.vertices[(target + arr_of_arr.length - 1) % arr_of_arr.length].X() 
                     + (1 - frac) * poly_in.vertices[target].X();
        var int1_y = frac * poly_in.vertices[(target + arr_of_arr.length - 1) % arr_of_arr.length].Y() 
                     + (1 - frac) * poly_in.vertices[target].Y();
    
        var int1 = b1.create('point', [int1_x, int1_y], {name: 'Int1', size: 6, color: 'blue'});
    
        // arr_of_right_poly.splice(flag, 0, [int1.X(),int1.Y()]);
        arr_of_right_poly.splice(1, 0, [int1.X(),int1.Y()]);
        arr_of_left_poly.pop();
        arr_of_left_poly.push([int1.X(),int1.Y()]);
    
        poly_left.vertices.map(function(x){ b1.removeObject(x)});
        poly_right.vertices.map(function(x){ b1.removeObject(x)});
        b1.removeObject(poly_left);
        b1.removeObject(poly_right);
    
        var poly_left = b1.create('polygon', arr_of_left_poly, {hasInnerPoints: true, color: 'pink'});
        var poly_right = b1.create('polygon', arr_of_right_poly, {hasInnerPoints: true, color: 'blue'});
        poly_left.vertices.map(function(x){ b1.removeObject(x)});
        poly_left = sortClockwise_and_rotate(poly_left, stem_in, 'pink');
        poly_right.vertices.map(function(x){ b1.removeObject(x)});
        poly_right = sortClockwise_and_rotate(poly_right, stem_in, 'blue');

        window.pl = poly_left;
        window.pr = poly_right;
        
        // poly_left.setLabel("PLPLPL");
        // poly_right.setLabel("PRPRPRPR");
        b1.removeObject(tri);
        b1.removeObject(int1);
    }
    return [poly_left, poly_right];
}

var drawTreeRec = function(itern, poly_in, stem_in, centroid, strokeWidth_in) {
    if (poly_in.Area() < 500) { 
      poly_in.remove() ; 
      return }

    polyopts = {hasInnerPoints: true, color: 'red'}

    //var childrenPolys = bisecPoly(poly_in, stem_in, start_ep_2_no_in);
    var childrenVerts = bisectPolyMGN(poly_in, stem_in) ;
    window.dbgchildrenVerts = childrenVerts ;
    var poly_left = b1.create('polygon', childrenVerts[0], polyopts) ;
    var poly_right = b1.create('polygon', childrenVerts[1], polyopts) ;

    poly_left.setLabel("polyL"+itern)
    poly_right.setLabel("polyR"+itern)

    var cen_left = computeCentroidArea(poly_left);
    var cen_right = computeCentroidArea(poly_right);

    //var newStem = findNewStem(poly_left, poly_right, stem_in, strokeWidth_in);
    var newStem = findNewStemMGN(stem_in, centroid, strokeWidth_in);

    poly_in.remove() ;

    drawTreeRec(itern + 1, poly_left, newStem, cen_left, 7 * strokeWidth_in/11);
    drawTreeRec(itern + 1, poly_right, newStem, cen_right, 7 * strokeWidth_in/11);
    
}//drawTreeRec

//computeCentroidArea(poly)

drawTreeRec(0, poly, stem, computeCentroidArea(poly), 25);
//
//
//    var polyopts = {hasInnerPoints: true, color: 'red'} ;
//    var poly_left = b1.create('polygon', polys[0], polyopts) ;
//    var poly_right = b1.create('polygon', polys[1], polyopts) ;
