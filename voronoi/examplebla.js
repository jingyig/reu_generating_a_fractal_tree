var b1 = JXG.JSXGraph.initBoard('jxgbox', {boundingbox: [-4, 2, 6, -4]});
var p1 = b1.create('point',[0,0], {name:'X', size:4});
var p2 = b1.create('point',[2,-1], {name:'B', size:4});
var p3 = b1.create('point',[-2,-3], {name:'C', size:4});
var p4 = b1.create('point',[-1,-1], {name:'D', size:4});
var p5 = b1.create('point',[3,1], {name:'E', size:4});

var poly = b1.create('polygon',["X","B","C","D","E"], { borders:{strokeColor:'black'} });
var poly = b1.create('polygon',[p1,p2,p3,p4,p5], { borders:{strokeColor:'black'} });