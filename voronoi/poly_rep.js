// // Example_1
// var b1 = JXG.JSXGraph.initBoard('jxgbox', {boundingbox: [-4, 2, 6, -4]});
// var p1 = b1.create('point',[0,0], {name:'X', size:4});
// var p2 = b1.create('point',[2,-1], {name:'B', size:4});
// var p3 = b1.create('point',[-2,-3], {name:'C', size:4});
// var p4 = b1.create('point',[-1,-1], {name:'D', size:4});
// var p5 = b1.create('point',[3,1], {name:'E', size:4});

// var poly = b1.create('polygon',["X","B","C","D","E"], { borders:{strokeColor:'black'} });
// var poly = b1.create('polygon',[p1,p2,p3,p4,p5], { borders:{strokeColor:'black'} });

// // Example_2
// var p = [[0.0, 2.0], [2.0, 1.0], [4.0, 6.0], [1.0, 3.0]];

// var pol = b1.create('polygon', p, {hasInnerPoints: true});
  
// Transfer relative coordinate into absolute coordinate
function convertToAbsolute(path){
    var x0,y0,x1,y1,x2,y2,segs = path.pathSegList;
    for (var x=0,y=0,i=0,len=segs.numberOfItems;i<len;++i){
      var seg = segs.getItem(i), c=seg.pathSegTypeAsLetter;
      if (/[MLHVCSQTA]/.test(c)){
        if ('x' in seg) x=seg.x;
        if ('y' in seg) y=seg.y;
      }else{
        if ('x1' in seg) x1=x+seg.x1;
        if ('x2' in seg) x2=x+seg.x2;
        if ('y1' in seg) y1=y+seg.y1;
        if ('y2' in seg) y2=y+seg.y2;
        if ('x'  in seg) x+=seg.x;
        if ('y'  in seg) y+=seg.y;
        switch(c){
          case 'm': segs.replaceItem(path.createSVGPathSegMovetoAbs(x,y),i);                   break;
          case 'l': segs.replaceItem(path.createSVGPathSegLinetoAbs(x,y),i);                   break;
          case 'h': segs.replaceItem(path.createSVGPathSegLinetoHorizontalAbs(x),i);           break;
          case 'v': segs.replaceItem(path.createSVGPathSegLinetoVerticalAbs(y),i);             break;
          case 'c': segs.replaceItem(path.createSVGPathSegCurvetoCubicAbs(x,y,x1,y1,x2,y2),i); break;
          case 's': segs.replaceItem(path.createSVGPathSegCurvetoCubicSmoothAbs(x,y,x2,y2),i); break;
          case 'q': segs.replaceItem(path.createSVGPathSegCurvetoQuadraticAbs(x,y,x1,y1),i);   break;
          case 't': segs.replaceItem(path.createSVGPathSegCurvetoQuadraticSmoothAbs(x,y),i);   break;
          case 'a': segs.replaceItem(path.createSVGPathSegArcAbs(x,y,seg.r1,seg.r2,seg.angle,seg.largeArcFlag,seg.sweepFlag),i);   break;
          case 'z': case 'Z': x=x0; y=y0; break;
        }
      }
      // Record the start of a subpath
      if (c=='M' || c=='m') x0=x, y0=y;
    }
}
  
  
var svg = document.childNodes[1].childNodes[2].childNodes[3];
var path = document.createElementNS("http://www.w3.org/2000/svg", "path");
  
var vxs_str_m = "m 53.672618,99.02976 15.119048,-30.994046 45.357144,-14.363096 49.89285,11.339287 31.75,27.214285 -6.04761,49.89286 -63.5,58.96428 -51.404766,-3.77976 -24.946427,-30.2381 z";

var vxs_str_2 = "m 77.270167,230.13071 -17.357791,26.87658 113.665534,29.1163 -46.47408,-45.91416 -8.39894,-23.51701 -29.676219,2.23972 z";
// Create a path object
path.setAttribute("d", vxs_str_m);
path.setAttribute("style", "fill:none;stroke:#000000;stroke-width:0.264583px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1");
convertToAbsolute(path);
svg.appendChild(path);

// get the arr of vxs
var mystring = path.getAttribute('d');
var arr = mystring.split(" ").filter(function (st) { return(st.length > 1) }).map(function(x) { return(x * 1)});
var arr_of_arr = [];

// set xmin xmax ymin ymax to prepare for boundingbx boarder
var xmin = arr[0];
var xmax = arr[0];
var ymin = -arr[1];
var ymax = -arr[1];

for(i = 0; i < arr.length; ++i) {
    if (arr[i] < xmin) {
        xmin = arr[i];
    }
    if (arr[i] > xmax) {
        xmax = arr[i];
    }
    var pair = [arr[i++], -arr[i]];
    if (-arr[i] < ymin) {
        ymin = -arr[i];
    }
    if (-arr[i] > ymax) {
        ymax = -arr[i];
    }
    arr_of_arr.push(pair);
}

var b1 = JXG.JSXGraph.initBoard('jxgbox', { grid: true
                                            , boundingbox: [xmin - 10
                                                            , ymax + 10
                                                            , xmax + 10
                                                            , ymin - 10]
                                            , keepaspectratio: true});

  

// An array containing four numbers describing the left
// , top, right and bottom boundary of the board in user coordinates

var poly = b1.create('polygon', arr_of_arr, {hasInnerPoints: true});
// Label vertices
for(i = 0; i < poly.vertices.length - 1; ++i) {
    var str = "p";
    str = str.concat(i);
    poly.vertices[i].setLabel(str);
}

// Set the stem point
var start_ep_1_no = 2;
var start_ep_2_no = (start_ep_1_no + 1) % arr_of_arr.length;

var edpt1 = poly.vertices[start_ep_1_no];
var edpt2 = poly.vertices[start_ep_2_no];
var gamma = 0.7;

var stem_x_coord = gamma * edpt2.X() + (1 - gamma) * edpt1.X();
var stem_y_coord = gamma * edpt2.Y() + (1 - gamma) * edpt1.Y();

var stem = b1.create('point', [stem_x_coord, stem_y_coord], {name: 'Stem', size: 6, color: 'blue'});

// Cut the polygon into half
var arr_of_left_poly = [];
var arr_of_right_poly = arr_of_arr.slice();
arr_of_left_poly.push([stem.X(), stem.Y()]);
arr_of_left_poly.push([edpt2.X(), edpt2.Y()]);
arr_of_right_poly.splice(start_ep_2_no, 1, [stem.X(), stem.Y()]);
var target;
var flag;
// find target
for(i = 1; i < arr_of_arr.length -  2; ++i) {
    arr_of_left_poly.push([poly.vertices[(start_ep_2_no + i) % arr_of_arr.length].X()
                           , poly.vertices[(start_ep_2_no + i) % arr_of_arr.length].Y()]);
    if(i > 1) {
        if(start_ep_2_no < arr_of_right_poly.length - 1) {
            arr_of_right_poly.splice(start_ep_2_no + 1, 1);
            flag = start_ep_2_no + 1;
        } else {
            arr_of_right_poly.splice(0 , 1);
            flag = 0;
        }
    }

    var poly_left = b1.create('polygon', arr_of_left_poly, {hasInnerPoints: true, color: 'pink'});
    var poly_right = b1.create('polygon', arr_of_right_poly, {hasInnerPoints: true, color: 'blue'});
    
    if(poly_left.Area() >= poly_right.Area()){
        target = (start_ep_2_no + i) % arr_of_arr.length;
        break;
    } else {
        poly_left.vertices.map(function(x){ b1.removeObject(x)});
        poly_right.vertices.map(function(x){ b1.removeObject(x)});
        b1.removeObject(poly_left);
        b1.removeObject(poly_right);
    }
}//for

// // find the final intersection point on the line
if(poly_left.Area() > poly_right.Area()) {
    var tri = b1.create('polygon', [stem, poly.vertices[target]
                                    , poly.vertices[(target + arr_of_arr.length - 1) % arr_of_arr.length]]);
    var frac = (poly_left.Area() - poly_right.Area()) / (2 * tri.Area());

    var int1_x = frac * poly.vertices[(target + 8) % 9].X() + (1 - frac) * poly.vertices[target].X();
    var int1_y = frac * poly.vertices[(target + 8) % 9].Y() + (1 - frac) * poly.vertices[target].Y();

    var int1 = b1.create('point', [int1_x, int1_y], {name: 'Int1', size: 6, color: 'blue'});

    arr_of_right_poly.splice(flag, 0, [int1.X(),int1.Y()]);
    arr_of_left_poly.pop();
    arr_of_left_poly.push([int1.X(),int1.Y()]);

    poly_left.vertices.map(function(x){ b1.removeObject(x)});
    poly_right.vertices.map(function(x){ b1.removeObject(x)});
    b1.removeObject(poly_left);
    b1.removeObject(poly_right);

    var poly_left = b1.create('polygon', arr_of_left_poly, {hasInnerPoints: true, color: 'pink'});
    var poly_right = b1.create('polygon', arr_of_right_poly, {hasInnerPoints: true, color: 'blue'});

    b1.removeObject(tri);
    b1.removeObject(int1);
}

var computeCentroid = function (poly) {
    // construct triangles
    var tris = [];
    for (i = 1; i < poly.vertices.length - 2; ++i) {
        var tri = b1.create('polygon', [poly.vertices[0]
                                        , poly.vertices[i]
                                        , poly.vertices[i+1]], {hasInnerPoints: true
                                                                , color: 'red'});
        
        // compute the centroid of triangles
        tri_cen_x = 1/3 * (poly.vertices[0].X() 
                           + poly.vertices[i].X()
                           + poly.vertices[i+1].X());
        tri_cen_y = 1/3 * (poly.vertices[0].Y() 
                           + poly.vertices[i].Y()
                           + poly.vertices[i+1].Y());
        tri.centroid = [tri_cen_x, tri_cen_y];

        // // test
        // var str = 'centroid';
        // str = str.concat(i);
        // var tri_centroid = b1.create('point', [tri_cen_x, tri_cen_y], {name: str, size: 6, color: 'blue'});

        tris.push(tri);
    }//for

    // add them up ??????????????, get centroid of poly
    var cen_x_numerator = 0;
    var cen_y_numerator = 0;;
    var denominator = 0;
    for (i = 0; i < tris.length ; ++i) {
        cen_x_numerator += (tris[i].centroid[0] + tris[(i+1)%tris.length].centroid[0]) 
                            * (tris[i].centroid[0] * tris[(i+1)%tris.length].centroid[1]
                               - tris[(i+1)%tris.length].centroid[0] * tris[i].centroid[1]);
        cen_y_numerator += (tris[i].centroid[1] + tris[(i+1)%tris.length].centroid[1]) 
                            * (tris[i].centroid[0] * tris[(i+1)%tris.length].centroid[1]
                               - tris[(i+1)%tris.length].centroid[0] * tris[i].centroid[1]);
        denominator += (tris[i].centroid[0] * tris[(i+1)%tris.length].centroid[1]
                              - tris[(i+1)%tris.length].centroid[0] * tris[i].centroid[1]);
        
    }

    var cen_x = cen_x_numerator / (3 * denominator) ;
    var cen_y = cen_y_numerator / (3 * denominator) ;

    var centroid = b1.create('point', [cen_x, cen_y], {name: 'Centroid', size: 6, color: 'blue'});

    // delete all the tris
    for(i = 0; i < tris.length; ++i) {
        b1.removeObject(tris[i]);
    }
}//conputeCentroid

var computeCentroidArea = function (poly) {
    // construct triangles
    var tris = [];
    for (i = 1; i < poly.vertices.length - 2; ++i) {
        var tri = b1.create('polygon', [poly.vertices[0]
                                        , poly.vertices[i]
                                        , poly.vertices[i+1]], {hasInnerPoints: true
                                                                , color: 'red'});
        
        // compute the centroid of triangles
        tri_cen_x = 1/3 * (poly.vertices[0].X() 
                           + poly.vertices[i].X()
                           + poly.vertices[i+1].X());
        tri_cen_y = 1/3 * (poly.vertices[0].Y() 
                           + poly.vertices[i].Y()
                           + poly.vertices[i+1].Y());
        tri.centroid = [tri_cen_x, tri_cen_y];

        tris.push(tri);
    }//for

    // add them up, get centroid of poly
    var cen_x = 0;
    var cen_y = 0;;
    for (i = 0; i < tris.length ; ++i) {
        cen_x += tris[i].centroid[0] * tris[i].Area();
        cen_y += tris[i].centroid[1] * tris[i].Area();
        
    }

    cen_x /= poly.Area() ;
    cen_y /= poly.Area() ;

    poly.centroid = b1.create('point', [cen_x, cen_y]
                                     , {name: 'CentroidArea', size: 6, color: 'blue'});

    // delete all the tris
    for(i = 0; i < tris.length; ++i) {
        b1.removeObject(tris[i]);
    }
}//conputeCentroidArea

// computeCentroid(poly_left);
// computeCentroid(poly_right);
// computeCentroid(poly);
computeCentroidArea(poly_left);
computeCentroidArea(poly_right);
computeCentroidArea(poly);


// var volume = function(x,y, r0) {
//     var l1 = ;
//     var l2 = ;
//     var l3 = ;
//     return (l1+l2) * (r0 / Math.sqrt(2)) ** 2 * Math.PI
//              + l3 * r0 ** 2 * Math.PI;
// }
// volume.method('minimal', function(x,y)) {
//     this
// }
var newStem;

var findNewStem = function() {
    var ns_x = 1/3 * (poly_left.centroid.X() + poly_right.centroid.X() + stem.X());
    var ns_y = 1/3 * (poly_left.centroid.Y() + poly_right.centroid.Y() + stem.Y());
    newStem = b1.create('point', [ns_x, ns_y]
                                   , {name: 'Stem', size: 6, color: 'blue'});
    
    // create a branch
    var branch = b1.create('segment', [stem, newStem],{strokeWidth: 10});   
}

findNewStem();



// outline
var drawTreeRec = function(poly) {
    // Initial condition

    // cut into half

    // computeCentroidArea

    // findNewStem

    // call drawTreeRec recursively

    
}

// test
poly_right.vertices[0].strokeColor('pink');
poly_right.vertices[1].strokeColor('yellow');
poly_right.vertices[2].strokeColor('green');
poly_right.vertices[3].strokeColor('orange');
poly_right.vertices[4].strokeColor('purple');
poly_right.vertices[5].strokeColor('blue');
poly_right.vertices[6].strokeColor('white');
