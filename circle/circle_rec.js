var svg = document.childNodes[0];
var drawCircleRec = function (x, y, r) {
    if (r < 1/9) {
        return;
    }
    var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    var randomColor = Math.floor(Math.random()*16777215).toString(16);
    circle.setAttribute("cx", x);
    circle.setAttribute("cy", y);
    circle.setAttribute("r", r);
    circle.setAttribute("fill", "#" + randomColor);
    // circle.setAttribute("fill", "black");
    svg.appendChild(circle);
    drawCircleRec(x-r, y+r, 1/2 * r);
    drawCircleRec(r+x, y+r, 1/2 * r);
}

drawCircleRec(450,0,90);