var svg = document.childNodes[0];
for (i = 0; i < 50; ++i) {
    var circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    circle.setAttribute("r", 3);
    circle.setAttribute("cx", 9*i);
    circle.setAttribute("cy", 9*i);
    circle.setAttribute("fill", "pink");
    svg.appendChild(circle);
}