var svg = document.childNodes[0];

var concat = String.prototype.concat ;
var sqrt2 = Math.sqrt(2) ;
var rec = function(x, y, l, r) {
   if (l/6 < 0.25) { return; }
   var path = document.createElementNS("http://www.w3.org/2000/svg", 
"path");
   var str = "M ";
   str = str.concat(x - l, " ", y - l, " l ", l," ", l, " l ", l, " ", -l);
   path.setAttribute("d", str);
   path.setAttribute("stroke", "red");
   path.setAttribute("stroke-width", l/6);
   path.setAttribute("fill", "none");
   path.setAttribute("transform", "rotate(".concat(r, " ", x," ", y, ")"));
   svg.appendChild(path);

   if(r == 0) {
     rec(x - l, y - l, 1/2 * l, r - 45);
     rec(x + l, y - l, 1/2 * l, r + 45);
   } else if (r == -45) {
     rec(x - sqrt2*l, y, 1/2 * l, r - 45);
     rec(x, y - sqrt2*l, 1/2 * l, r + 45);
   } else if (r == 45) {
     rec(x + sqrt2*l, y, 1/2 * l, r - 45);
     rec(x, y - sqrt2*l, 1/2 * l, r + 45);
   }
}
rec(150, 200, 50, 0);