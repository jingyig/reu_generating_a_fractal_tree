var svg = document.childNodes[0];

var makestem = function () {
  var stem = document.createElementNS("http://www.w3.org/2000/svg" ,"path");
  stem.setAttribute("d","M 50 50 l 50 50 l 50 -50");
  stem.setAttribute("stroke-width", 5);
  stem.setAttribute("stroke", "red");
  stem.setAttribute("fill", "none");
  return(stem) }
  
var g = document.createElementNS("http://www.w3.org/2000/svg" ,"g");
g.setAttribute("transform","translate(100,100)")
svg.appendChild(g);
g.appendChild(makestem());

var rec = function(g, size, angle, scale) {
  if (size < 1) { return; }
  var gl = document.createElementNS("http://www.w3.org/2000/svg" ,"g");
  var gr = document.createElementNS("http://www.w3.org/2000/svg" ,"g");
  gl.setAttribute("transform",
    "".concat(
      "translate(",0,",0)",
      "scale(",scale,",",scale,") rotate(-",angle,",100,100) "
      
      )) ;
  gl.appendChild(makestem());
  gr.setAttribute("transform",
    "".concat(
      "translate(",100,",",0,") ",
      "scale(",scale,",",scale,") rotate(",angle,",100,100)")) ;
  gr.appendChild(makestem());
  g.appendChild(gl) ;
  g.appendChild(gr) ;
  rec(gl,size/2,angle,scale) ;
  rec(gr,size/2,angle,scale) }

rec(g,100,60,0.5)

var g2 = document.createElementNS("http://www.w3.org/2000/svg" ,"g");
g2.setAttribute("transform","translate(350,100)")
svg.appendChild(g2);
g2.appendChild(makestem());
rec(g2,100,30,0.5)
