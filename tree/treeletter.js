var letterob = {};
var generator_ob = function(object,length, diameter) {
    object.diameter = diameter;
    if(diameter < 1) {
        return {};
    }
    object.length = length;
    object.leftbranch = generator_ob({}
                                     , 1/2 * length 
                                     , 1/2 * diameter);
    object.leftangle = 1/4 * Math.PI;
    object.rightbranch = generator_ob({}
                                      , 1/2 * length 
                                      , 1/2 * diameter);
    object.rightangle = 3/4 * Math.PI;
    return object;
}
letterob = generator_ob({},40,10);


var letterarr = [];
var generator_arr = function(arr, length, diameter) {
    arr.push(diameter);
    if (diameter < 1) {
        return [];
    }
    arr.push(length);
    arr.push(generator_arr([]
                           , 1/2 * length
                           , 1/2 * diameter));
    arr.push(1/4 * Math.PI);
    arr.push(generator_arr([]
                           , 1/2 * length
                           , 1/2 * diameter));
    arr.push(3/4 * Math.PI);
    return arr;

}
letterarr = generator_arr([], 40, 10);

var letter_to_figure_arr = function(arr) {

}